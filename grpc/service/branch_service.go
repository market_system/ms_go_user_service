package service

import (
	"context"

	"gitlab.com/market_system/ms_go_user_service/config"
	"gitlab.com/market_system/ms_go_user_service/genproto/user_service"
	"gitlab.com/market_system/ms_go_user_service/grpc/client"
	"gitlab.com/market_system/ms_go_user_service/models"
	"gitlab.com/market_system/ms_go_user_service/pkg/logger"
	"gitlab.com/market_system/ms_go_user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type BranchService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*user_service.UnimplementedBranchServiceServer
}

func NewBranchService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *BranchService {
	return &BranchService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *BranchService) Create(ctx context.Context, req *user_service.CreateBranch) (resp *user_service.Branch, err error) {

	u.log.Info("-----------CreateBranch----------", logger.Any("req", req))

	id, err := u.strg.Branch().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateBranch ->Branch->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Branch().GetByID(ctx, &user_service.BranchPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("GetByIdBranch->GetById", logger.Error(err))
		return
	}

	return
}

func (u *BranchService) GetByID(ctx context.Context, req *user_service.BranchPrimaryKey) (resp *user_service.Branch, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Branch().GetByID(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Branch->GetByID", logger.Error(err))
		return
	}

	return
}

func (u *BranchService) GetList(ctx context.Context, req *user_service.GetListBranchRequest) (*user_service.GetListBranchResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Branch().GetList(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Branch->GetList", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *BranchService) Update(ctx context.Context, req *user_service.UpdateBranch) (resp *user_service.Branch, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Branch().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Branch->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Branch().GetByID(ctx, &user_service.BranchPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Branch->GetByID", logger.Error(err))
		return
	}

	return
}

func (i *BranchService) UpdatePatch(ctx context.Context, req *user_service.UpdatePatchBranch) (resp *user_service.Branch, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Branch().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchBranch--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Branch().GetByID(ctx, &user_service.BranchPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Branch->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *BranchService) Delete(ctx context.Context, req *user_service.BranchPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	err = u.strg.Branch().Delete(ctx, &user_service.BranchPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Branch->Delete", logger.Error(err))
		return
	}

	return
}
