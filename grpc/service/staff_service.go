package service

import (
	"context"
	"errors"

	"gitlab.com/market_system/ms_go_user_service/config"
	"gitlab.com/market_system/ms_go_user_service/genproto/user_service"
	"gitlab.com/market_system/ms_go_user_service/grpc/client"
	"gitlab.com/market_system/ms_go_user_service/models"
	"gitlab.com/market_system/ms_go_user_service/pkg/logger"
	"gitlab.com/market_system/ms_go_user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type StaffService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*user_service.UnimplementedStaffServiceServer
}

func NewStaffService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *StaffService {
	return &StaffService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *StaffService) Create(ctx context.Context, req *user_service.CreateStaff) (resp *user_service.Staff, err error) {

	u.log.Info("-----------CreateStaff----------", logger.Any("req", req))

	id, err := u.strg.Staff().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateStaff ->Staff->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Staff().GetByID(ctx, &user_service.StaffPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("GetByIdStaff->GetById", logger.Error(err))
		return
	}

	return
}
func (u *StaffService) Login(ctx context.Context, req *user_service.LoginStaff) (resp *user_service.Staff, err error) {

	u.log.Info("-----------CreateStaff----------", logger.Any("req", req))

	resp, err = u.strg.Staff().GetByID(ctx, &user_service.StaffPrimaryKey{Login: req.Login})
	if err != nil {
		u.log.Error("Login -> GetByIdStaff -> GetById", logger.Error(err))
		return
	}

	if resp.Password != req.Password {
		u.log.Info("Invalid Password!")
		return nil, errors.New("Invalid Password!")
	}

	return
}

func (u *StaffService) GetByID(ctx context.Context, req *user_service.StaffPrimaryKey) (resp *user_service.Staff, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Staff().GetByID(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Staff->GetByID", logger.Error(err))
		return
	}

	return
}

func (u *StaffService) GetList(ctx context.Context, req *user_service.GetListStaffRequest) (*user_service.GetListStaffResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Staff().GetList(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Staff->GetList", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *StaffService) Update(ctx context.Context, req *user_service.UpdateStaff) (resp *user_service.Staff, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Staff().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Staff->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Staff().GetByID(ctx, &user_service.StaffPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Staff->GetByID", logger.Error(err))
		return
	}

	return
}

func (i *StaffService) UpdatePatch(ctx context.Context, req *user_service.UpdatePatchStaff) (resp *user_service.Staff, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Staff().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchStaff--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Staff().GetByID(ctx, &user_service.StaffPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Staff->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *StaffService) Delete(ctx context.Context, req *user_service.StaffPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	err = u.strg.Staff().Delete(ctx, &user_service.StaffPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Staff->Delete", logger.Error(err))
		return
	}

	return
}
