package service

import (
	"context"

	"gitlab.com/market_system/ms_go_user_service/config"
	"gitlab.com/market_system/ms_go_user_service/genproto/user_service"
	"gitlab.com/market_system/ms_go_user_service/grpc/client"
	"gitlab.com/market_system/ms_go_user_service/models"
	"gitlab.com/market_system/ms_go_user_service/pkg/logger"
	"gitlab.com/market_system/ms_go_user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type MarketService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*user_service.UnimplementedMarketServiceServer
}

func NewMarketService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *MarketService {
	return &MarketService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *MarketService) Create(ctx context.Context, req *user_service.CreateMarket) (resp *user_service.Market, err error) {

	u.log.Info("-----------CreateMarket----------", logger.Any("req", req))

	id, err := u.strg.Market().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateMarket ->Market->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Market().GetByID(ctx, &user_service.MarketPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("GetByIdMarket->GetById", logger.Error(err))
		return
	}

	return
}

func (u *MarketService) GetByID(ctx context.Context, req *user_service.MarketPrimaryKey) (resp *user_service.Market, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Market().GetByID(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Market->GetByID", logger.Error(err))
		return
	}

	return
}

func (u *MarketService) GetList(ctx context.Context, req *user_service.GetListMarketRequest) (*user_service.GetListMarketResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Market().GetList(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Market->GetList", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *MarketService) Update(ctx context.Context, req *user_service.UpdateMarket) (resp *user_service.Market, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Market().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Market->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Market().GetByID(ctx, &user_service.MarketPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Market->GetByID", logger.Error(err))
		return
	}

	return
}

func (i *MarketService) UpdatePatch(ctx context.Context, req *user_service.UpdatePatchMarket) (resp *user_service.Market, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Market().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchMarket--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Market().GetByID(ctx, &user_service.MarketPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *MarketService) Delete(ctx context.Context, req *user_service.MarketPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	err = u.strg.Market().Delete(ctx, &user_service.MarketPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Market->Delete", logger.Error(err))
		return
	}

	return
}
