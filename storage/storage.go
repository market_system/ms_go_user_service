package storage

import (
	"context"

	"gitlab.com/market_system/ms_go_user_service/genproto/user_service"
	"gitlab.com/market_system/ms_go_user_service/models"
)

type StorageI interface {
	CloseDB()
	Branch() BranchRepoI
	Market() MarketRepoI
	Staff() StaffRepoI
}
type BranchRepoI interface {
	Create(context.Context, *user_service.CreateBranch) (string, error)
	GetByID(context.Context, *user_service.BranchPrimaryKey) (*user_service.Branch, error)
	GetList(context.Context, *user_service.GetListBranchRequest) (*user_service.GetListBranchResponse, error)
	Update(context.Context, *user_service.UpdateBranch) (int64, error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(context.Context, *user_service.BranchPrimaryKey) error
}
type MarketRepoI interface {
	Create(context.Context, *user_service.CreateMarket) (string, error)
	GetByID(context.Context, *user_service.MarketPrimaryKey) (*user_service.Market, error)
	GetList(context.Context, *user_service.GetListMarketRequest) (*user_service.GetListMarketResponse, error)
	Update(context.Context, *user_service.UpdateMarket) (int64, error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(context.Context, *user_service.MarketPrimaryKey) error
}
type StaffRepoI interface {
	Create(context.Context, *user_service.CreateStaff) (string, error)
	GetByID(context.Context, *user_service.StaffPrimaryKey) (*user_service.Staff, error)
	GetList(context.Context, *user_service.GetListStaffRequest) (*user_service.GetListStaffResponse, error)
	Update(context.Context, *user_service.UpdateStaff) (int64, error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(context.Context, *user_service.StaffPrimaryKey) error
}
