package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_user_service/genproto/user_service"
	"gitlab.com/market_system/ms_go_user_service/models"
	"gitlab.com/market_system/ms_go_user_service/pkg/helper"
)

type staffRepo struct {
	db *pgxpool.Pool
}

func NewStaffRepo(db *pgxpool.Pool) *staffRepo {
	return &staffRepo{
		db: db,
	}
}

func (r *staffRepo) Create(ctx context.Context, req *user_service.CreateStaff) (string, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "staff"(id, name,surname,phone_number,login, password, user_type,market_id, created_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		&id,
		&req.Name,
		&req.Surname,
		&req.PhoneNumber,
		&req.Login,
		&req.Password,
		&req.UserType,
		&req.MarketId,
	)

	if err != nil {
		return "", err
	}

	return id, nil
}

func (r *staffRepo) GetByID(ctx context.Context, req *user_service.StaffPrimaryKey) (*user_service.Staff, error) {
	var (
		query  string
		filter string
		args   string

		id           sql.NullString
		name         sql.NullString
		surname      sql.NullString
		phone_number sql.NullString
		login        sql.NullString
		password     sql.NullString
		userType     sql.NullString
		marketId     sql.NullString
		created_at   sql.NullString
	)

	if len(req.Id) > 0 {
		filter = " WHERE id = $1 "
		args = req.Id
	} else if len(req.Login) > 0 {
		filter = " WHERE login = $1 "
		args = req.Login
	}

	query = `
		SELECT
			id,
			name,
			surname,
			phone_number,
			login,
			password,
			user_type,
			market_id,
			created_at
		FROM "staff"
	`

	query += filter

	err := r.db.QueryRow(ctx, query, args).Scan(
		&id,
		&name,
		&surname,
		&phone_number,
		&login,
		&password,
		&userType,
		&marketId,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &user_service.Staff{
		Id:          id.String,
		Name:        name.String,
		Surname:     surname.String,
		PhoneNumber: phone_number.String,
		Login:       login.String,
		Password:    password.String,
		UserType:    userType.String,
		MarketId:    marketId.String,
		CreatedAt:   created_at.String,
	}, nil
}

func (r *staffRepo) GetList(ctx context.Context, req *user_service.GetListStaffRequest) (*user_service.GetListStaffResponse, error) {

	var (
		resp   = &user_service.GetListStaffResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			surname,
			phone_number,
			login,
			password,
			user_type,
			market_id,
			created_at,
			updated_at
		FROM "staff"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			name         sql.NullString
			surname      sql.NullString
			phone_number sql.NullString
			login        sql.NullString
			password     sql.NullString
			userType     sql.NullString
			marketId     sql.NullString
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&surname,
			&phone_number,
			&login,
			&password,
			&userType,
			&marketId,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Staffs = append(resp.Staffs, &user_service.Staff{
			Id:          id.String,
			Name:        name.String,
			Surname:     surname.String,
			PhoneNumber: phone_number.String,
			Login:       login.String,
			Password:    password.String,
			UserType:    userType.String,
			MarketId:    marketId.String,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}

	return resp, nil
}

func (r *staffRepo) Update(ctx context.Context, req *user_service.UpdateStaff) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"staff"
		SET
			name = :name,
			surname = :surname,
			phone_number = :phone_number,
			login = :login,
			password = :password,
			user_type = :user_type,
			market_id = :market_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"surname":      req.Surname,
		"phone_number": req.PhoneNumber,
		"login":        req.Login,
		"password":     req.Password,
		"user_type":    req.UserType,
		"market_id":    req.MarketId,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil

}

func (r *staffRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"staff"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (r *staffRepo) Delete(ctx context.Context, req *user_service.StaffPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM staff WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
