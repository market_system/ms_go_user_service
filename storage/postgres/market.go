package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_user_service/genproto/user_service"
	"gitlab.com/market_system/ms_go_user_service/models"
	"gitlab.com/market_system/ms_go_user_service/pkg/helper"
)

type marketRepo struct {
	db *pgxpool.Pool
}

func NewMarketRepo(db *pgxpool.Pool) *marketRepo {
	return &marketRepo{
		db: db,
	}
}

func (r *marketRepo) Create(ctx context.Context, req *user_service.CreateMarket) (string, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "market"(id, name,branch_id, created_at)
		VALUES ($1, $2, $3,NOW())
	`

	_, err := r.db.Exec(ctx, query,
		&id,
		&req.Name,
		&req.BranchId,
	)

	if err != nil {
		return "", err
	}

	return id, nil
}

func (r *marketRepo) GetByID(ctx context.Context, req *user_service.MarketPrimaryKey) (*user_service.Market, error) {
	var (
		query string

		id         sql.NullString
		name       sql.NullString
		branchId   sql.NullString
		created_at sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			branch_id,
			created_at
		FROM "market"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&branchId,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &user_service.Market{
		Id:        id.String,
		Name:      name.String,
		BranchId:  branchId.String,
		CreatedAt: created_at.String,
	}, nil
}

func (r *marketRepo) GetList(ctx context.Context, req *user_service.GetListMarketRequest) (*user_service.GetListMarketResponse, error) {

	var (
		resp   = &user_service.GetListMarketResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			branch_id,
			created_at,
			updated_at
		FROM "market"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			name       sql.NullString
			branchId   sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&branchId,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Markets = append(resp.Markets, &user_service.Market{
			Id:        id.String,
			Name:      name.String,
			BranchId:  branchId.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}

	return resp, nil
}

func (r *marketRepo) Update(ctx context.Context, req *user_service.UpdateMarket) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"market"
		SET
			name = :name,
			branch_id = :branch_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.Id,
		"name":      req.Name,
		"branch_id": req.BranchId,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *marketRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"market"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (r *marketRepo) Delete(ctx context.Context, req *user_service.MarketPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM market WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
