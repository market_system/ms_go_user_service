CREATE TABLE "branch" (
  "id" UUID PRIMARY KEY NOT NULL,
  "name" varchar,
  "branch_code" varchar NOT NULL,
  "address" varchar,
  "phone_number" varchar,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "market" (
  "id" UUID PRIMARY KEY NOT NULL,
  "name" varchar,
  "branch_id" UUID,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "staff" (
  "id" UUID PRIMARY KEY NOT NULL,
  "name" varchar,
  "surname" varchar,
  "phone_number" varchar,
  "login" varchar UNIQUE,
  "password" varchar,
  "user_type" varchar,
  "market_id" UUID,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

ALTER TABLE "market" ADD FOREIGN KEY ("branch_id") REFERENCES "branch" ("id");

ALTER TABLE "staff" ADD FOREIGN KEY ("market_id") REFERENCES "market" ("id");